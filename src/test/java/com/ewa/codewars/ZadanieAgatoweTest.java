package com.ewa.codewars;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ZadanieAgatoweTest {

    @Test
    public void ewaTest() {
        assertEquals("{a=2, b=1, c=1}", ZadanieAgatowe.zliczWystapienia("abca"));
        assertEquals("{a=2, b=1, c=1}", ZadanieAgatowe.zliczWystapienia("abc a"));

    }

    @Test
    public void lukaszTest() {
        assertEquals("{a=2, b=1, c=1}", ZadanieAgatowe.sprytneZliczWystapienia("abcabcabcdef def qwerty"));
    }
}
