package com.ewa.codewars;

public class Solution {

    public static double solution(double[] arrVal, String[] arrUnit) {
        for(int i=0;i<2;i++){
            switch(arrUnit[i]){
                case "g": arrVal[i] = arrVal[i]/1000.0; break;
                case "mg":  arrVal[i] = arrVal[i]/1000000.0; break;
                case "μg": arrVal[i] = arrVal[i]/1000000000.0; break;
                case "lb": arrVal[i] = arrVal[i]*0.453592; break;
                default: break;
            }
        }
        switch(arrUnit[2]){
            case "cm": arrVal[2] = arrVal[2]/100.0; break;
            case "mm":  arrVal[2] = arrVal[2]/1000.0; break;
            case "μm": arrVal[2] = arrVal[2]/1000000.0; break;
            case "ft": arrVal[2] = arrVal[2]*0.3048; break;
            default: break;
        }
        double g = (double) Math.pow(10,11);
        double f = 6.67*arrVal[0]*arrVal[1]/(arrVal[2]*arrVal[2]*g);
        return f;

    }
}
