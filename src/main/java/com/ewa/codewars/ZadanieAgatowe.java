package com.ewa.codewars;

import java.util.Map;
import java.util.TreeMap;

public class ZadanieAgatowe {
    public static String zliczWystapienia (String str){

        Map<String, Integer> dictionary = new TreeMap();

        for(String e : str.replaceAll("\\s+","").split("")) {
            if (dictionary.containsKey(e)) {
                dictionary.put(e, dictionary.get(e) + 1);
            } else {
                dictionary.put(e, 1);
            }
        }

        return dictionary.toString();
    }

    public static String sprytneZliczWystapienia(String input) {
    }
}
